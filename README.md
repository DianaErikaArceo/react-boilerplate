# Prerequisites
Nodejs version: `^6.10.0`

# Installation
From terminal use commands npm or yarn(prefer):
`npm install` or `ỳarn`.

# Configuration
Folder *conf/* have folders development, production and test.
They have a index.json file configuration.
* Here can modify ~/endpoints.
* Validations.
* Error codes.
* Some basic components modifications.

# Development
Open terminal and type `yarn dev`, this command up Chrome browser with localhost:xxxx for start to development mode.
All files you need to update are on folder *conf/development* and *src/*.
On development mode you can use Chrome [Redux DevTools Extension](http://extension.remotedev.io/ "Redux DevTools")

On second terminal run `yarn test`, if you want test in real time when are you coding. This is testing framework called "Jest" on `package.json` is configuration.

## Testing
Open terminal and type `yarn test:coverage` for run all test with.
* Connection api.
* Render

# Build, compile output SPA application.
Open terminal and type `yarn build`, all files are out on folder *dist/*.

# Compatibility javascript when code is build.
Tested browser compatibility with http://www.browserstack.com with next minimum requirements:
- Windows 7, IE10.
- Snow Leopard, Safari 5.1.
- IOS 5.1, iPhone 4S and Ipad 2.
- Android 4.4.
