import Axios from 'axios';
import Conf from 'conf';

export default class API {

    constructor(apiUrl) {
        this.baseURL = apiUrl;

        this.Connection = Axios.create({
            baseURL: this.baseURL,
            timeout: 30000,
        });
    }

    getConnection() {
        return this.Connection;
    }

    get(uri, callback) {
        this.Connection.get(uri)
            .then(callback)
            .catch(this.handleError);
    }

    post(uri, requestBody, callback) {
        this.Connection.post(uri, requestBody)
            .then(callback)
            .catch(this.handleError);
    }

    put(uri, requestBody, callback) {
        this.Connection.put(uri, requestBody)
            .then(callback)
            .catch(this.handleError);
    }

    getWithoutErrors(uri, callback) {
        this.Connection.get(uri, { validateStatus: (status) => {
            callback(status);
        } })
        .catch((error) => {
            if (error.response) {
                callback(error.response.status);
            } else {
                callback(404);
            }
        });
    }

    handleError(error) {
        if (error.request) {
            console.warn('Error request:', error.request);
        }
        if (Conf.debug) {
            console.warn(error);
            throw new Error(error);
        }
    }
}
