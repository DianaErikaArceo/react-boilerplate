import { combineReducers } from 'redux';

import Home from './components/_layouts/Home/homeReducer';
import Loading from './components/_helpers/Loading/loadingReducer';

export default combineReducers({
    Home,
    Loading,
});
